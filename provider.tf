terraform {
  required_providers {
    google = {
      source = "hashicorp/google"
      version = "4.74.0"
    }
  }
}

provider "google" {
  project = var.gcp_project     # Default project, which can be overwritten in every resource
  region  = "us-central1"       # Default region, which can be overwritten in every resource
  zone    = "us-central1-c"     # Default zone, which can be overwritten in every resource
}
