resource "google_compute_instance" "my_vm_instance" {
  name         = "terraform-instance"
  machine_type = "e2-micro"

  boot_disk {
    initialize_params {
      image = "debian-cloud/debian-11"
    }
  }
  allow_stopping_for_update = "true"

  network_interface {
    # A default network is created for all GCP projects
    # network = "default"
    network = google_compute_network.my_vpc_network.self_link      # link to another resource
    access_config {
    }
  }
}

resource "google_compute_network" "my_vpc_network" {
  name                    = "terraform-network"
  auto_create_subnetworks = "true"
}
