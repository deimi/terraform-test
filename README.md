# Instructions
https://registry.terraform.io/providers/hashicorp/google/latest/docs/guides/getting_started  
https://www.freecodecamp.org/news/terraform-syntax-for-beginners/  


# Install

```bash
wget -O- https://apt.releases.hashicorp.com/gpg | sudo gpg --dearmor -o /usr/share/keyrings/hashicorp-archive-keyring.gpg
echo "deb [signed-by=/usr/share/keyrings/hashicorp-archive-keyring.gpg] https://apt.releases.hashicorp.com $(lsb_release -cs) main" | sudo tee /etc/apt/sources.list.d/hashicorp.list
sudo apt update && sudo apt install terraform
```

# Usage

```bash
terraform init          # initialize terraform and install plugins and providers
GOOGLE_APPLICATION_CREDENTIALS=./gcp-test-key.json terraform plan       # dry run and prints what will be done
GOOGLE_APPLICATION_CREDENTIALS=./gcp-test-key.json terraform apply
GOOGLE_APPLICATION_CREDENTIALS=./gcp-test-key.json terraform destroy
```
